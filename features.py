import itertools
import logging
from collections import Counter, defaultdict

from edmonds import Arc

logger = logging.getLogger("dp")


class BFeatures(object):
    def __init__(self, data):
        """
        Feature generation and extraction for basic model
        :param data: list<list<node>>, list of trees.
        :return:
        """
        self.weights = defaultdict(int)
        self.data = data
        self.orig_features = []

    def extract(self):
        """
        Extracts the various features to be extracted from training data.
        :return:
        """
        for tree_ind in xrange(len(self.data)):
            tree = self.data[tree_ind]
            temp_features = []
            for i in range(len(tree)):
                temp_features.extend(self.t_feat(i, tree, tree[i][2]))
            count_features = Counter(temp_features)
            self.orig_features.append(count_features)

    def features_stats(self):
        """
        Logs statistics of the features extracted (for further analysis by hand)
        :return:
        """
        stat_list = [k[-3:] for k in self.weights.iterkeys()]
        return Counter(stat_list)

    def extract_single(self, tree):
        """
        Extract features for a single tree
        :param tree: list<node>
        :return: dict<string, int>, returns a dictionary of features(as string) and the number of apparels.
        """
        temp_features = []
        for i in range(len(tree)):
            temp_features.extend(self.t_feat(i, tree, tree[i][2]))
        return Counter(temp_features)

    @staticmethod
    def t_feat(i, tree, h):
        """
        Extract features from a given parent and child.
        :param i: int, index of current word in tree.
        :param tree: list<node>, list of the tree nodes.
        :param h: int, index of current head.
        :return: tuple<string>, tuple of features as string. tuple was chosen because of about 40% performance increase vs list in list extension.
        """
        c = tree[i]
        p = tree[h]
        pword = p[0]
        cword = c[0]
        ppos = p[1]
        cpos = c[1]
        return (pword + '_' + ppos + "_f01",
                pword + "_f02",
                ppos + "_f03",
                cword + '_' + cpos + "_f04",
                cword + "_f05",
                cpos + "_f06",
                ppos + '_' + cword + '_' + cpos + "_f08",
                pword + '_' + ppos + '_' + cpos + "_f10",
                ppos + '_' + cpos + "_f13")

    def extrapolate_tree(self, tree):
        """
        Creates a clique from a given tree (list of headless nodes), with the sum of weights*frequency of each feature.
        :param tree:
        :return:
        """
        edges = []
        tr = range(len(tree))
        for i, j in itertools.product(tr, tr):
            temp_features = self.t_feat(i, tree, j)
            sum_f = sum(self.weights.get(term, 0) for term in temp_features)
            edges.append(Arc(i, sum_f, j))
        return edges


class CFeatures(BFeatures):
    def __init__(self, data):
        super(CFeatures, self).__init__(data)

    @staticmethod
    def t_feat(i, tree, h):
        c = tree[i]
        p = tree[h]
        plpos = clpos = "^"
        crpos = prpos = "$"
        if i > 1 and h < len(tree) - 1:
            clpos = tree[i - 1][1]
            prpos = tree[h + 1][1]
        if h > 1 and i < len(tree) - 1:
            crpos = tree[i + 1][1]
            plpos = tree[h - 1][1]
        pword = p[0]
        cword = c[0]
        ppos = p[1]
        cpos = c[1]

        dist = str(abs(h - i))
        return (  # pword + '_' + ppos + "_f01",  # removal improved accuracy
                # pword + "_f02", # removal improved accuracy
                # ppos + "_f03", # removal improved accuracy
                # cword + '_' + cpos + "_f04",  # removal hasn't effected accuracy
                # cword + "_f05", # removal improved accuracy
                # cpos + "_f06",  # removal improved accuracy
                pword + '_' + ppos + '_' + cword + '_' + cpos + "_f07",  # improves
                ppos + '_' + cword + '_' + cpos + "_f08",  # improves
                pword + '_' + cword + '_' + cpos + "_f09",  # improves
                pword + '_' + ppos + '_' + cpos + "_f10",  # improves
                pword + '_' + ppos + '_' + cword + "_f11",  # improves
                pword + '_' + cword + "_f12",  # improves
                # ppos + '_' + cpos + "_f13",  # removal improved accuracy
                # dist + "_f14",  # removal improved accuracy
                dist + '_' + ppos + "_f15",  # improves
                # dist + '_' + cpos + "_f16", # removal improved accuracy
                dist + '_' + cpos + '_' + ppos + "_f17",  # improves
                clpos + '_' + cpos + '_' + ppos + '_' + prpos + "_f18",  # improves
                clpos + '_' + cpos + '_' + ppos + "_f19",  # improves
                # cpos + '_' + ppos + '_' + prpos + "_f20",  # removal improved accuracy
                clpos + '_' + ppos + '_' + prpos + "_f21",  # improves
                clpos + '_' + cpos + '_' + prpos + "_f22",  # improves
                # cpos + '_' + prpos + "_f23",  # removal improved accuracy
                clpos + '_' + ppos + "_f24",  # improves
                clpos + '_' + prpos + "_f25",  # improves
                crpos + '_' + cpos + '_' + ppos + '_' + plpos + "_f26",  # improves
                crpos + '_' + cpos + '_' + ppos + "_f27",  # improves
                cpos + '_' + ppos + '_' + plpos + "_f28",  # improves
                # crpos + '_' + ppos + '_' + plpos + "_f29",  # removal improved accuracy
                crpos + '_' + cpos + '_' + plpos + "_f30",  # improves
                # cpos + '_' + plpos + "_f31",  # removal improved accuracy
                # crpos + '_' + ppos + "_f32"  # removal improved accuracy
                # crpos + '_' + plpos + "_f33"  # removal improved accuracy
                clpos + '_' + crpos + '_' + plpos + '_' + prpos + "_f34",  # improves
                crpos + '_' + plpos + '_' + prpos + "_f35",  # improves
                clpos + '_' + plpos + '_' + prpos + "_f36"  # improves
            # clpos + '_' + crpos + '_' + prpos + "_f37"  # removal improved accuracy
            # clpos + '_' + crpos + '_' + plpos + "_f38"  # removal improved accuracy
                )
