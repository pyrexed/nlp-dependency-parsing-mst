from collections import defaultdict, namedtuple

Arc = namedtuple('Arc', ('tail', 'weight', 'head'))


def mst_dag(arcs, sink):
    good_arcs = []
    quotient_map = {arc[0]: arc[0] for arc in arcs}
    quotient_map[sink] = sink
    while True:
        max_arc_by_tail_rep = {}
        successor_rep = {}
        for arc in arcs:
            if arc[0] == sink:
                continue
            tail_rep = quotient_map[arc[0]]
            head_rep = quotient_map[arc[2]]
            if tail_rep == head_rep:
                continue
            if (tail_rep not in max_arc_by_tail_rep) or (max_arc_by_tail_rep[tail_rep][1] < arc[1]):
                max_arc_by_tail_rep[tail_rep] = arc
                successor_rep[tail_rep] = head_rep
        cycle_reps = find_cycle(successor_rep, sink)
        if cycle_reps is None:
            good_arcs.extend(max_arc_by_tail_rep.values())
            return spanning_arborescence(good_arcs, sink)
        good_arcs.extend(max_arc_by_tail_rep[cycle_rep] for cycle_rep in cycle_reps)
        cycle_rep_set = set(cycle_reps)
        cycle_rep = cycle_rep_set.pop()
        quotient_map = {node: cycle_rep if node_rep in cycle_rep_set else node_rep for node, node_rep in quotient_map.items()}


def find_cycle(successor, sink):
    visited = {sink}
    for node in successor:
        cycle = []
        while node not in visited:
            visited.add(node)
            cycle.append(node)
            node = successor[node]
        if node in cycle:
            return cycle[cycle.index(node):]
    return None


def spanning_arborescence(arcs, sink):
    arcs_by_head = defaultdict(list)
    for arc in arcs:
        if arc[0] == sink:
            continue
        arcs_by_head[arc[2]].append(arc)
    solution_arc_by_tail = {}
    stack = arcs_by_head[sink]
    while stack:
        arc = stack.pop()
        if arc[0] in solution_arc_by_tail:
            continue
        solution_arc_by_tail[arc[0]] = arc
        stack.extend(arcs_by_head[arc[0]])
    return solution_arc_by_tail
