import gc
import logging
import os
import sys
from collections import namedtuple

import cPickle as pickle

from edmonds import mst_dag
from features import BFeatures, CFeatures

logger = logging.getLogger('dp')
logger.setLevel(logging.DEBUG)
ch = logging.FileHandler("dp.log", mode='a')
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)

"""
tn is the node class, it has 5 data members: word, pos - part of speech, h - the head of the node (effective parent)
example of instantiation: a = tn("The","DT",6)
"""
tn = namedtuple("node", ["word", "pos", "h"])


class DP(object):
    """
    Dependency Parser
    """

    def __init__(self, path, training_data, comp):
        """
        Instantiating Dependency parser class
        :param path: string, current path for loading pickle files
        :param training_data: list<list<node>>, the training data read from the labeled data
        :param comp: boolean, if True uses the complex feature extraction class, else uses the basic
        :return:
        """
        self.pkl_file = os.path.join(path, "model.pkl")
        self.training_data = training_data
        if comp:
            logger.debug("Using complex features.")
            self.feature_class = CFeatures(training_data)
        else:
            logger.debug("Using basic features.")
            self.feature_class = BFeatures(training_data)
        if os.path.isfile(self.pkl_file):
            self.feature_class.weights, self.feature_class.features = pickle.load(open(self.pkl_file, "rb"))
        else:
            self.feature_class.extract()

    def save(self):
        """
        Saves weights, and feature data to pickle file.
        :return:
        """
        pickle.dump((self.feature_class.weights, self.feature_class.orig_features), open(self.pkl_file, "wb"))

    def perceptron(self, n=20):
        """
        Matches the perceptron algorithm, in the exercise pdf
        :param n: int, number of iterations to perform
        :return:
        """
        for i in range(n):
            for tree_ind in xrange(len(self.training_data)):
                gc.disable()
                tree = self.training_data[tree_ind]
                tree_clique = self.feature_class.extrapolate_tree(tree)
                max_graph = mst_dag(tree_clique, 0)
                max_tree = self.treeify(max_graph, tree)
                if not compare(max_tree, tree):
                    orig_feat = self.feature_class.orig_features[tree_ind]
                    max_feat = self.feature_class.extract_single(max_tree)
                    for feat in orig_feat:
                        self.feature_class.weights[feat] += orig_feat[feat] - max_feat[feat]
                gc.enable()

    @staticmethod
    def treeify(g, tree):
        """
        Takes the graph (list of Arcs-as defined in edmonds.py) and converts them to list of nodes.
        :param g: list<Arc>
        :param tree: list<Node>
        :return: list<Node>
        """
        new_tree = [None] * len(tree)
        new_tree[0] = tn("^", "ROOT", 0)
        for edge in g.itervalues():
            node = tree[edge.tail]
            new_tree[edge.tail] = tn(node[0], node[1], edge[2])
        return new_tree

    def inference(self, trees):
        """
        Finds the maximum spanning tree using the learnt weights and the Edmonds algorithm
        :param trees: list<node>, list of nodes with missing heads.
        :return: list<node>, list of nodes with inferred heads.
        """
        gc.disable()
        max_trees = []
        for tree in trees:
            tree_clique = self.feature_class.extrapolate_tree(tree)
            max_graph = mst_dag(tree_clique, 0)
            max_trees.append(self.treeify(max_graph, tree))
        gc.enable()
        return max_trees


def main(testing=True, n=2, comp=True):
    """
    sets the environment and loads files, and dependency parsing class.
    :param testing: boolean, whether we use test.labeled or competition.
    :param n: int, number of perceptron iteration,
    :param comp: boolean, whether or not to use the complex or basic features.
    :return:
    """
    logger.debug("Starting DP with %d perceptron iteration, and testing argument set to %s." % (n, str(testing)))
    # for timing
    s_time = os.times()[-1]
    # local path for files
    path = os.path.dirname(os.path.abspath(__file__))
    # locating relevant files
    comp_path = os.path.join(path, "comp.unlabeled")
    train_path = os.path.join(path, "train.labeled")
    test_path = os.path.join(path, "test.labeled")

    # loading labeled data (training set)
    train_trees = load_labeled(train_path)
    # instantiating the dependecy parser (DP) with the training trees and a flag for using complex vs basic features
    dp = DP(path, train_trees, comp)
    dp.perceptron(n)
    logger.debug("Feature Stats: %s" % str(dp.feature_class.features_stats()))
    f_sum = sum(dp.feature_class.weights.values())
    logger.debug("Average weight: %.2f" % (float(f_sum) / len(dp.feature_class.weights)))
    if testing:
        test_trees = load_labeled(test_path)
        inferred_trees = dp.inference(test_trees)
        acc_conf(test_trees, inferred_trees)
    else:
        comp_set = load_unlabeled(comp_path)
        comp_trees = dp.inference(comp_set)
        name = "%d_" % n
        name += "complex" if comp else "basic"
        write_labeled(comp_trees, path, name)
    tot_time = os.times()[-1] - s_time
    logger.debug("Total running time: %.2f" % tot_time)


def acc_conf(test_trees, inferred_trees):
    """
    Calculates the accuracy and other statistics.
    :param test_trees:
    :param inferred_trees:
    :return:
    """
    acc_stats = []
    assert len(test_trees) == len(inferred_trees)
    for i in range(len(inferred_trees)):
        test_tree = test_trees[i]
        inf_tree = inferred_trees[i]
        assert len(test_tree) == len(inf_tree)
        local_correct_count = 0
        for j in range(1, len(test_tree)):
            local_correct_count += int(test_tree[j][2] == inf_tree[j][2])
        acc_stats.append((local_correct_count, len(test_tree), i))
    tot_count = sum(k[1] for k in acc_stats)
    correct_count = sum(k[0] for k in acc_stats)
    max_acc = max(100 * float(k[0]) / k[1] for k in acc_stats)
    min_acc = min(100 * float(k[0]) / k[1] for k in acc_stats)
    logger.debug("Num of Correct: %d" % correct_count)
    logger.debug("Num of Total: %d" % tot_count)
    logger.debug("Correctness: %.2f%%" % (100 * float(correct_count) / tot_count))
    logger.debug("Maximum Accuracy: %.2f%%" % max_acc)
    logger.debug("Minimum Accuracy: %.2f%%" % min_acc)
    # c = count()
    # for ind in [ts[2] for ts in filter(lambda tt: tt[0] == 0, acc_stats)]:
    #     logger.debug("inf: " + "".join(["{:^10}".format(k[2]) for k in inferred_trees[ind]]))
    #     logger.debug("tst: " + "".join(["{:^10}".format(k[2]) for k in test_trees[ind]]))
    #     logger.debug("POS: " + "".join(["{:^10}".format(k[1]) for k in inferred_trees[ind]]))
    #     logger.debug("word: " + "".join(["{:^10}".format(k[0]) for k in inferred_trees[ind]]))
    #     if c.next() >= 10:
    #         break


def write_labeled(trees, path, name):
    """
    Writes a new file with the labeled data

    :param trees: list<list<tn(node)>>, list of tree, tree= list of node, node as defined above in tn.
    :param name: string, addendum to the name of the file.
    :param path: string, local path
    :return:
    """
    file_path = os.path.join(path, "comp_" + name + ".labeled")
    newline = ""
    with open(file_path, "wb") as f:
        for tree in trees:
            f.write(newline)
            for i in range(1, len(tree)):
                node = tree[i]
                f.write(newline + "%d\t%s\t_\t%s\t_\t_\t%d\t_\t_\t_" % (i, node[0], node[1], node[2]))
                newline = "\r\n"
        f.write("\r\n\r\n")


def load_labeled(filepath):
    """
    Loads a file with labeled data (training/test)
    :param filepath: String, the path to the file to parse.
    :return: list<list<tn(node)>>, list of tree, tree= list of node, node as defined above in tn
    """
    trees = []
    gc.disable()
    with open(filepath, "rb") as f:
        nodes = [tn("^", "ROOT", 0)]
        for line in f:
            if line == "\r\n":
                trees.append(nodes)
                nodes = [tn("^", "ROOT", 0)]
                continue
            tokens = line.split("\t")
            nodes.append(tn(tokens[1], tokens[3], int(tokens[6])))
    gc.enable()
    return trees


def load_unlabeled(filepath):
    """
    Loads a file with unlabeled data (comp)
    :param filepath: String, the path to the file to parse.
    :return: list<list<tn(node)>>, list of tree, tree= list of node, node as defined above in tn, the nodes are missing head data
    """
    trees = []
    with open(filepath, "rb") as f:
        nodes = [tn("^", "ROOT", 0)]
        for line in f:
            if line == "\r\n":
                trees.append(nodes)
                nodes = [tn("^", "ROOT", 0)]
                continue
            tokens = line.split("\t")
            nodes.append(tn(tokens[1], tokens[3], -1))
    return trees


def compare(nodes1, nodes2):
    """
    Compares two trees (for the compare line in the perceptron algorithm) by checking the head arcs
    for each one of the nodes on each tree.
    :param nodes1: list<node>
    :param nodes2: list<node>
    :return:
    """
    for i in range(1, len(nodes1)):
        if nodes1[i][2] != nodes2[i][2]:
            return False
    return True


if __name__ == "__main__":
    """
    The following line are for calling the main() function and providing
    profiling statistics about run-times and function calls.
    """
    try:
        if len(sys.argv) == 4:
            test = False
            if sys.argv[1].lower() == "test":
                test = True
            elif sys.argv[1].lower() == "comp":
                test = False
            else:
                raise Exception()
            n_iter = int(sys.argv[2])
            if sys.argv[3].lower() == "complex":
                comp = True
            elif sys.argv[3].lower() == "basic":
                comp = False
            else:
                raise Exception()
            main(test, n_iter, comp)
        else:
            raise Exception()
    except:
        print "Usage: python main.py <'test'|'comp'> <iterations> <'complex'|'basic'>"
